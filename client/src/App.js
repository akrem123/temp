import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import HomePage from './Components/HomePage';
import About from './Components/About';
import Contact from './Components/Contact';
import Listings from './Components/Listings';
import Single from './Components/Single';

class App extends Component {
  render() {
    return (

      <Router>

                <Route exact path="/" component={HomePage} />
                  <Switch>
                      <Route exact path="/About" component={About} />
                      <Route exact path="/Contact" component={Contact} />
                      <Route exact path="/Listings" component={Listings} />
                      <Route exact path="/Single" component={Single} />
                  </Switch>

      </Router>
      
    )
  }
}
export default App;