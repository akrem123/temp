import React, { Component } from 'react';


 class Footer extends Component {
    render() {
        return (
            <div>
               <footer className="footer">
		<div className="container">
			<div className="row">
				
			

				<div className="col-lg-3 footer_col">
					<div className="footer_col_title">
						<div className="logo_container">
							<a href="#">
								<div className="logo">
									<img src="images/logo.png" alt=""/>
									<span>Sublimmo</span>
								</div>
							</a>
						</div>
					</div>
					<div className="footer_social">
						<ul className="footer_social_list">
							
							<li className="footer_social_item"><a href="#"><i className="fab fa-facebook-f"></i></a></li>
							<li className="footer_social_item"><a href="#"><i className="fab fa-twitter"></i></a></li>
							<li className="footer_social_item"><a href="#"><i className="fab fa-instagram"></i></a></li>

							
							
						</ul>
					</div>
					<div className="footer_about">
						<p>Lorem ipsum dolor sit amet, cons ectetur  quis ferme adipiscing elit. Suspen dis se tellus eros, placerat quis ferme ntum et, viverra sit amet lacus. Nam gravida  quis ferme semper augue.</p>
					</div>
				</div>
				
			

				<div className="col-lg-3 footer_col">
					<div className="footer_col_title">useful links</div>
					<ul className="footer_useful_links">
						<li className="useful_links_item"><a href="#">Listings</a></li>
						<li className="useful_links_item"><a href="#">Favorite Cities</a></li>
						<li className="useful_links_item"><a href="#">Clients Testimonials</a></li>
						<li className="useful_links_item"><a href="#">Featured Listings</a></li>
						<li className="useful_links_item"><a href="#">Properties on Offer</a></li>
						<li className="useful_links_item"><a href="#">Services</a></li>
						<li className="useful_links_item"><a href="#">News</a></li>
						<li className="useful_links_item"><a href="#">Our Agents</a></li>
					</ul>
				</div>

				
				<div className="col-lg-3 footer_col">
					<div className="footer_col_title">say hello</div>
					<div className="footer_contact_form_container">
						<form id="footer_contact_form" className="footer_contact_form" action="post">
							<input id="contact_form_name" className="input_field contact_form_name" type="text" placeholder="Name" required="required" data-error="Name is required."/>
							<input id="contact_form_email" className="input_field contact_form_email" type="email" placeholder="E-mail" required="required" data-error="Valid email is required."/>
							<textarea id="contact_form_message" className="text_field contact_form_message" name="message" placeholder="Message" required="required" data-error="Please, write us a message."></textarea>
							<button id="contact_send_btn" type="submit" className="contact_send_btn trans_200" value="Submit">send</button>
						</form>
					</div>
				</div>

				

				<div className="col-lg-3 footer_col">
					<div className="footer_col_title">contact info</div>
					<ul className="contact_info_list">
						<li className="contact_info_item d-flex flex-row">
							<div><div className="contact_info_icon"><img src="images/placeholder.svg" alt=""/></div></div>
							<div className="contact_info_text">4127 Raoul Wallenber 45b-c Gibraltar</div>
						</li>
						<li className="contact_info_item d-flex flex-row">
							<div><div className="contact_info_icon"><img src="images/phone-call.svg" alt=""/></div></div>
							<div className="contact_info_text">2556-808-8613</div>
						</li>
						<li className="contact_info_item d-flex flex-row">
							<div><div className="contact_info_icon"><img src="images/message.svg" alt=""/></div></div>
							<div className="contact_info_text"><a href="mailto:contactme@gmail.com?Subject=Hello" target="_top">contactme@gmail.com</a></div>
						</li>
						
					</ul>
				</div>

			</div>
		</div>
	</footer>
	<div className="credits">
		<span>
Copyright &copy; All rights reserved 2020
</span>
	</div>
            </div>
        )
    }
}

export default Footer;