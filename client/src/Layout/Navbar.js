import React, { Component } from 'react'

class Navbar extends Component {
    render() {
        return (
            <div>
                <header className="header trans_300">
		<div className="container">
			<div className="row">
				<div className="col">
					<div className="header_container d-flex flex-row align-items-center trans_300">

					

						<div className="logo_container">
							<a href="/">
								<div className="logo">
									<img src="images/logo.png" alt=""/>
									<span>Sublimmo</span>
								</div>
							</a>
						</div>
						

						<nav className="main_nav">
							<ul className="main_nav_list">
								<li className="main_nav_item"><a href="/">home</a></li>
								<li className="main_nav_item"><a href="/About">about us</a></li>
								<li className="main_nav_item"><a href="/Listings">listings</a></li>
								
								<li className="main_nav_item"><a href="/Contact">contact</a></li>
							</ul>
						</nav>
						

						<div className="phone_home text-center">
							<span>+0080 234 567 84441</span>
						</div>
						
						<div className="hamburger_container menu_mm">
							<div className="hamburger menu_mm">
								<i className="fas fa-bars trans_200 menu_mm"></i>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

		<div className="menu menu_mm">
			<ul className="menu_list">
				<li className="menu_item">
					<div className="container">
						<div className="row">
							<div className="col">
								<a href="/">home</a>
							</div>
						</div>
					</div>
				</li>
				<li className="menu_item">
					<div className="container">
						<div className="row">
							<div className="col">
								<a href="/About">about us</a>
							</div>
						</div>
					</div>
				</li>
				<li className="menu_item">
					<div className="container">
						<div className="row">
							<div className="col">
								<a href="/Listings">listings</a>
							</div>
						</div>
					</div>
				</li>
				
				<li className="menu_item">
					<div className="container">
						<div className="row">
							<div className="col">
								<a href="/Contact">contact</a>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>

	</header>
            </div>
        )
    }
}

export default Navbar;