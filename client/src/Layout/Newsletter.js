import React, { Component } from 'react'

class Newsletter extends Component {
    render() {
        return (
            <div>
                <div className="newsletter">
		<div className="container">
			<div className="row row-equal-height">

				<div className="col-lg-6">
					<div className="newsletter_title">
						<h3>subscribe to our newsletter</h3>
						<span className="newsletter_subtitle">Get the latest offers</span>
					</div>
					<div className="newsletter_form_container">
						<form action="#">
							<div className="newsletter_form_content d-flex flex-row">
								<input id="newsletter_email" className="newsletter_email" type="email" placeholder="Your email here" required="required" data-error="Valid email is required."/>
								<button id="newsletter_submit" type="submit" className="newsletter_submit_btn trans_200" value="Submit">subscribe</button>
							</div>
						</form>
					</div>
				</div>

				<div className="col-lg-6">
					<a href="#">
						<div className="weekly_offer">
							<div className="weekly_offer_content d-flex flex-row">
								<div className="weekly_offer_icon d-flex flex-column align-items-center justify-content-center">
									<img src="images/prize.svg" alt=""/>
								</div>
								<div className="weekly_offer_text text-center">weekly offer</div>
							</div>
							<div className="weekly_offer_image" style={{backgroundImage:"url(images/weekly.jpg)"}}></div>
						</div>
					</a>
				</div>

			</div>
		</div>
	</div>
            </div>
        )
    }
}

export default Newsletter