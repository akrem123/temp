import React, { Component } from 'react'
import Footer from '../Layout/Footer'
import Newsletter from '../Layout/Newsletter';
import Navbar from '../Layout/Navbar';


 class HomePage extends Component {
    render() {
        return (
            <div>
                <div className="super_container"> 
                
                <div className="home">
		
	
		<div className="home_slider_container">
			<div className="owl-carousel owl-theme home_slider">

			
				<div className="owl-item home_slider_item">
					
					<div className="home_slider_background" style={{backgroundImage:"url(images/home_slider_bcg.jpg)"}}></div>
					<div className="home_slider_content_container text-center">
						<div className="home_slider_content">
							<h1 data-animation-in="flipInX" data-animation-out="animate-out fadeOut">Find Your Home</h1>
						</div>
					</div>
				</div>

				
				<div className="owl-item home_slider_item">
					
					<div className="home_slider_background" style={{backgroundImage:"url(images/slider_1.jpg)"}}></div>
					<div className="home_slider_content_container text-center">
						<div className="home_slider_content">
							<h1 data-animation-in="flipInX" data-animation-out="animate-out fadeOut">Find Your Home</h1>
						</div>
					</div>
				</div>

				
				<div className="owl-item home_slider_item">
					
					<div className="home_slider_background" style={{backgroundImage:"url(images/slider_2.jpg)"}}></div>
					<div className="home_slider_content_container text-center">
						<div className="home_slider_content">
							<h1 data-animation-in="flipInX" data-animation-out="animate-out fadeOut">Find Your Home</h1>
						</div>
					</div>
				</div>
			</div>
			
			
			<div className="home_slider_nav_left home_slider_nav d-flex flex-row align-items-center justify-content-end">
				<img src="images/nav_left.png" alt=""/>
			</div>

		</div>

	</div>
                
    <Navbar />


    <div className="search_box">
		<div className="container">
			<div className="row">
				<div className="col">

					<div className="search_box_outer">
						<div className="search_box_inner">

							
							<div className="search_box_title text-center">
								<div className="search_box_title_inner">
									<div className="search_box_title_icon d-flex flex-column align-items-center justify-content-center"><img src="images/search.png" alt=""/></div>
									<span>Search your home </span>
								</div>
							</div>

							
							<div className="search_arrow_box">
								<div className="search_arrow_box_inner">
									<div className="search_arrow_circle d-flex flex-column align-items-center justify-content-center"><span>Search it here</span></div>
									<img src="images/search_arrow.png" alt=""/>
								</div>
							</div>

							
							<form className="search_form" action="#">
								<div className="search_box_container">
									<ul className="dropdown_row clearfix">
										<li className="dropdown_item dropdown_item_5">
											<div className="dropdown_item_title">Keywords</div>
											<select name="keywords" id="keywords" className="dropdown_item_select">
												<option>Any</option>
												<option>Keyword 1</option>
												<option>Keyword 2</option>
											</select>
										</li>
										<li className="dropdown_item dropdown_item_5">
											<div className="dropdown_item_title">Property ID</div>
											<select name="property_ID" id="property_ID" className="dropdown_item_select">
												<option>Any</option>
												<option>ID 1</option>
												<option>ID 2</option>
											</select>
										</li>
										<li className="dropdown_item dropdown_item_5">
											<div className="dropdown_item_title">Property Status</div>
											<select name="property_status" id="property_status" className="dropdown_item_select">
												<option>Any</option>
												<option>Status 1</option>
												<option>Status 2</option>
											</select>
										</li>
										<li className="dropdown_item dropdown_item_5">
											<div className="dropdown_item_title">Location</div>
											<select name="property_location" id="property_location" className="dropdown_item_select">
												<option>Any</option>
												<option>Location 1</option>
												<option>Location 2</option>
											</select>
										</li>
										<li className="dropdown_item dropdown_item_5">
											<div className="dropdown_item_title">Property Type</div>
											<select name="property_type" id="property_type" className="dropdown_item_select">
												<option>Any</option>
												<option>Type 1</option>
												<option>Type 2</option>
											</select>
										</li>
									</ul>
								</div>

								<div className="search_box_container">
									<ul className="dropdown_row clearfix">
										<li className="dropdown_item dropdown_item_6">
											<div className="dropdown_item_title">Bedrooms no</div>
											<select name="bedrooms_no" id="bedrooms_no" className="dropdown_item_select">
												<option>Any</option>
												<option>1</option>
												<option>2</option>
											</select>
										</li>
										<li className="dropdown_item dropdown_item_6">
											<div className="dropdown_item_title">Bathrooms no</div>
											<select name="bathrooms_no" id="bathrooms_no" className="dropdown_item_select">
												<option>Any</option>
												<option>1</option>
												<option>2</option>
											</select>
										</li>
										<li className="dropdown_item dropdown_item_6">
											<div className="dropdown_item_title">Min Price</div>
											<select name="min_price" id="min_price" className="dropdown_item_select">
												<option>Any</option>
												<option>$10000</option>
												<option>$20000</option>
											</select>
										</li>
										<li className="dropdown_item dropdown_item_6">
											<div className="dropdown_item_title">Max Price</div>
											<select name="max_price" id="max_price" className="dropdown_item_select">
												<option>Any</option>
												<option>$1000000</option>
												<option>$2000000</option>
											</select>
										</li>
										<li className="dropdown_item dropdown_item_6">
											<div className="dropdown_item_title">Min Sq Ft</div>
											<select name="min_sq_ft" id="min_sq_ft" className="dropdown_item_select">
												<option>Any</option>
												<option>Any</option>
												<option>Any</option>
											</select>
										</li>
										<li className="dropdown_item dropdown_item_6">
											<div className="dropdown_item_title">Max Sq Ft</div>
											<select name="max_sq_ft" id="max_sq_ft" className="dropdown_item_select">
												<option>Any</option>
												<option>Any</option>
												<option>Any</option>
											</select>
										</li>
										<li className="dropdown_item">
											<div className="search_button">
												<input value="search" type="submit" className="search_submit_button"/>
											</div>
										</li>
									</ul>
								</div>

								<div className="search_features_container">
									<div className="search_features_trigger">
										<a href="#">Specific features</a>
									</div>
									<ul className="search_features clearfix">
										<li className="search_features_item">
											<div>
												<input type="checkbox" id="search_features_1" className="search_features_cb"/>
												<label for="search_features_1">Feature 1</label>
											</div>	
										</li>
										<li className="search_features_item">
											<div>
												<input type="checkbox" id="search_features_2" className="search_features_cb"/>
												<label for="search_features_2">Feature 2</label>
											</div>
										</li>
										<li className="search_features_item">
											<div>
												<input type="checkbox" id="search_features_3" className="search_features_cb"/>
												<label for="search_features_3">Feature 3</label>
											</div>
										</li>
										<li className="search_features_item">
											<div>
												<input type="checkbox" id="search_features_4" className="search_features_cb"/>
												<label for="search_features_4">Feature 4</label>
											</div>
										</li>
										<li className="search_features_item">
											<div>
												<input type="checkbox" id="search_features_5" className="search_features_cb"/>
												<label for="search_features_5">Feature 5</label>
											</div>
										</li>
										<li className="search_features_item">
											<div>
												<input type="checkbox" id="search_features_6" className="search_features_cb"/>
												<label for="search_features_6">Feature 6</label>
											</div>
										</li>
										<li className="search_features_item">
											<div>
												<input type="checkbox" id="search_features_7" className="search_features_cb"/>
												<label for="search_features_7">Feature 7</label>
											</div>
										</li>
										<li className="search_features_item">
											<div>
												<input type="checkbox" id="search_features_8" className="search_features_cb"/>
												<label for="search_features_8">Feature 8</label>
											</div>
										</li>
										<li className="search_features_item">
											<div>
												<input type="checkbox" id="search_features_9" className="search_features_cb"/>
												<label for="search_features_9">Feature 9</label>
											</div>
										</li>
										<li className="search_features_item">
											<div>
												<input type="checkbox" id="search_features_10" className="search_features_cb"/>
												<label for="search_features_10">Feature 10</label>
											</div>
										</li>
									</ul>
								</div>

							</form>
						</div>
					</div>

				</div>
			</div>
		</div>		
	</div>

    <div className="featured">
		<div className="container">
			<div className="row">
				<div className="col">
					<div className="section_title text-center">
						<h3>featured properties</h3>
						<span className="section_subtitle">See our best offers</span>
					</div>
				</div>
			</div>

			<div className="row featured_row">
				<div className="col-lg-4 featured_card_col">

					<div className="featured_card_container">
						<div className="card featured_card trans_300">
							<div className="featured_panel">featured</div>
							<img className="card-img-top" src="images/featured_1.jpg" alt="https://unsplash.com/@breather"/>
							<div className="card-body">
								<div className="card-title"><a href="listings_single.html">House in West California</a></div>
								<div className="card-text">Donec ullamcorper nulla non metus auctor fringi lla. Curabitur blandit tempus porttitor.</div>
								<div className="rooms">

									<div className="room">
										<span className="room_title">Bedrooms</span>
										<div className="room_content">
											<div className="room_image"><img src="images/bedroom.png" alt=""/></div>
											<span className="room_number">4</span>
										</div>
									</div>

									<div className="room">
										<span className="room_title">Bathrooms</span>
										<div className="room_content">
											<div className="room_image"><img src="images/shower.png" alt=""/></div>
											<span className="room_number">3</span>
										</div>
									</div>

									<div className="room">
										<span className="room_title">Area</span>
										<div className="room_content">
											<div className="room_image"><img src="images/area.png" alt=""/></div>
											<span className="room_number">7100 Sq Ft</span>
										</div>
									</div>

									<div className="room">
										<span className="room_title">Patio</span>
										<div className="room_content">
											<div className="room_image"><img src="images/patio.png" alt=""/></div>
											<span className="room_number">1</span>
										</div>
									</div>

									<div className="room">
										<span className="room_title">Garage</span>
										<div className="room_content">
											<div className="room_image"><img src="images/garage.png" alt=""/></div>
											<span className="room_number">2</span>
										</div>
									</div>

								</div>

								<div className="room_tags">
									<span className="room_tag"><a href="#">Hottub</a></span>
									<span className="room_tag"><a href="#">Swimming Pool</a></span>
									<span className="room_tag"><a href="#">Garden</a></span>
									<span className="room_tag"><a href="#">Patio</a></span>
									<span className="room_tag"><a href="#">Hard Wood Floor</a></span>
								</div>

							</div>
						</div>

						<div className="featured_card_box d-flex flex-row align-items-center trans_300">
							<img src="images/tag.svg" alt="https://www.flaticon.com/authors/lucy-g"/>
							<div className="featured_card_box_content">
								<div className="featured_card_price_title">For Sale</div>
								<div className="featured_card_price">$540,000</div>
							</div>
						</div>

					</div>

				</div>

				<div className="col-lg-4 featured_card_col">

					<div className="featured_card_container">
						<div className="card featured_card trans_300">
							<div className="featured_panel">featured</div>
							<img className="card-img-top" src="images/featured_2.jpg" alt="https://unsplash.com/@astute"/>
							<div className="card-body">
								<div className="card-title"><a href="listings_single.html">House in West California</a></div>
								<div className="card-text">Donec ullamcorper nulla non metus auctor fringi lla. Curabitur blandit tempus porttitor.</div>
								<div className="rooms">

									<div className="room">
										<span className="room_title">Bedrooms</span>
										<div className="room_content">
											<div className="room_image"><img src="images/bedroom.png" alt=""/></div>
											<span className="room_number">4</span>
										</div>
									</div>

									<div className="room">
										<span className="room_title">Bathrooms</span>
										<div className="room_content">
											<div className="room_image"><img src="images/shower.png" alt=""/></div>
											<span className="room_number">3</span>
										</div>
									</div>

									<div className="room">
										<span className="room_title">Area</span>
										<div className="room_content">
											<div className="room_image"><img src="images/area.png" alt=""/></div>
											<span className="room_number">7100 Sq Ft</span>
										</div>
									</div>

									<div className="room">
										<span className="room_title">Patio</span>
										<div className="room_content">
											<div className="room_image"><img src="images/patio.png" alt=""/></div>
											<span className="room_number">1</span>
										</div>
									</div>

									<div className="room">
										<span className="room_title">Garage</span>
										<div className="room_content">
											<div className="room_image"><img src="images/garage.png" alt=""/></div>
											<span className="room_number">2</span>
										</div>
									</div>

								</div>

								<div className="room_tags">
									<span className="room_tag"><a href="#">Hottub</a></span>
									<span className="room_tag"><a href="#">Swimming Pool</a></span>
									<span className="room_tag"><a href="#">Garden</a></span>
									<span className="room_tag"><a href="#">Patio</a></span>
									<span className="room_tag"><a href="#">Hard Wood Floor</a></span>
								</div>

							</div>
						</div>

						<div className="featured_card_box d-flex flex-row align-items-center trans_300">
							<img src="images/tag.svg" alt="https://www.flaticon.com/authors/lucy-g"/>
							<div className="featured_card_box_content">
								<div className="featured_card_price_title">For Sale</div>
								<div className="featured_card_price">$540,000</div>
							</div>
						</div>

					</div>

				</div>

				<div className="col-lg-4 featured_card_col">

					<div className="featured_card_container">
						<div className="card featured_card trans_300">
							<div className="featured_panel">featured</div>
							<img className="card-img-top" src="images/featured_3.jpg" alt="https://unsplash.com/@marcusneto"/>
							<div className="card-body">
								<div className="card-title"><a href="listings_single.html">House in West California</a></div>
								<div className="card-text">Donec ullamcorper nulla non metus auctor fringi lla. Curabitur blandit tempus porttitor.</div>
								<div className="rooms">

									<div className="room">
										<span className="room_title">Bedrooms</span>
										<div className="room_content">
											<div className="room_image"><img src="images/bedroom.png" alt=""/></div>
											<span className="room_number">4</span>
										</div>
									</div>

									<div className="room">
										<span className="room_title">Bathrooms</span>
										<div className="room_content">
											<div className="room_image"><img src="images/shower.png" alt=""/></div>
											<span className="room_number">3</span>
										</div>
									</div>

									<div className="room">
										<span className="room_title">Area</span>
										<div className="room_content">
											<div className="room_image"><img src="images/area.png" alt=""/></div>
											<span className="room_number">7100 Sq Ft</span>
										</div>
									</div>

									<div className="room">
										<span className="room_title">Patio</span>
										<div className="room_content">
											<div className="room_image"><img src="images/patio.png" alt=""/></div>
											<span className="room_number">1</span>
										</div>
									</div>

									<div className="room">
										<span className="room_title">Garage</span>
										<div className="room_content">
											<div className="room_image"><img src="images/garage.png" alt=""/></div>
											<span className="room_number">2</span>
										</div>
									</div>

								</div>

								<div className="room_tags">
									<span className="room_tag"><a href="#">Hottub</a></span>
									<span className="room_tag"><a href="#">Swimming Pool</a></span>
									<span className="room_tag"><a href="#">Garden</a></span>
									<span className="room_tag"><a href="#">Patio</a></span>
									<span className="room_tag"><a href="#">Hard Wood Floor</a></span>
								</div>

							</div>
						</div>

						<div className="featured_card_box d-flex flex-row align-items-center trans_300">
							<img src="images/tag.svg" alt="https://www.flaticon.com/authors/lucy-g"/>
							<div className="featured_card_box_content">
								<div className="featured_card_price_title">For Sale</div>
								<div className="featured_card_price">$540,000</div>
							</div>
						</div>

					</div>

				</div>

			</div>
		</div>
	</div>

    <div className="testimonials">
		<div className="testimonials_background_container prlx_parent">
			<div className="testimonials_background prlx"  style={{backgroundImage:"url(images/testimonials_background.jpg)"}}></div>
		</div>
		<div className="container">

			<div className="row">
				<div className="col">
					<div className="section_title text-center">
						<h3>clients testimonials</h3>
						<span className="section_subtitle">See our best offers</span>
					</div>
				</div>
			</div>

			<div className="row">
				<div className="col-lg-10 offset-lg-1">
					
					<div className="testimonials_slider_container">

					
						<div className="owl-carousel owl-theme testimonials_slider">
							
							
							<div className="owl-item">
								<div className="testimonials_item text-center">
									<p className="testimonials_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae neque libero. Vivamus vel interdum massa. Mauris ut felis vel diam pretium eleifend vel eu neque. Mauris a condimentum tortor. Cras nec molestie est. Nulla vel facilisis metus. Quisque tempus fermentum enim, in feugiat sem laoreet</p>
									<div className="testimonial_user">
										<div className="testimonial_image mx-auto">
											<img src="images/person.jpg" alt="https://unsplash.com/@remdesigns"/>
										</div>
										<div className="testimonial_name">natalie smith</div>
										<div className="testimonial_title">Client in California</div>
									</div>
								</div>
							</div>

							
							<div className="owl-item">
								<div className="testimonials_item text-center">
									<p className="testimonials_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae neque libero. Vivamus vel interdum massa. Mauris ut felis vel diam pretium eleifend vel eu neque. Mauris a condimentum tortor. Cras nec molestie est. Nulla vel facilisis metus. Quisque tempus fermentum enim, in feugiat sem laoreet</p>
									<div className="testimonial_user">
										<div className="testimonial_image mx-auto">
											<img src="images/person.jpg" alt="https://unsplash.com/@remdesigns"/>
										</div>
										<div className="testimonial_name">natalie smith</div>
										<div className="testimonial_title">Client in California</div>
									</div>
								</div>
							</div>

						
							<div className="owl-item">
								<div className="testimonials_item text-center">
									<p className="testimonials_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae neque libero. Vivamus vel interdum massa. Mauris ut felis vel diam pretium eleifend vel eu neque. Mauris a condimentum tortor. Cras nec molestie est. Nulla vel facilisis metus. Quisque tempus fermentum enim, in feugiat sem laoreet</p>
									<div className="testimonial_user">
										<div className="testimonial_image mx-auto">
											<img src="images/person.jpg" alt="https://unsplash.com/@remdesigns"/>
										</div>
										<div className="testimonial_name">natalie smith</div>
										<div className="testimonial_title">Client in California</div>
									</div>
								</div>
							</div>

						</div>

					</div>
				</div>
			</div>

		</div>
	</div>

    <div className="workflow">
		<div className="container">
			<div className="row">
				<div className="col">
					<div className="section_title text-center">
						<h3>see how we operate</h3>
						<span className="section_subtitle">What you need to do</span>
					</div>
				</div>
			</div>

			<div className="row workflow_row">
				<div className="workflow_rocket"><img src="images/rocket.png" alt=""/></div>

				
				<div className="col-lg-4 workflow_col">
					<div className="workflow_item">
						<div className="workflow_image_container d-flex flex-column align-items-center justify-content-center">
							<div className="workflow_image_background">
								<div className="workflow_circle_outer trans_200"></div>
								<div className="workflow_circle_inner trans_200"></div>
								<div className="workflow_num text-center trans_200"><span>01.</span></div>
							</div>
							<div className="workflow_image">
								<img src="images/workflow_1.png" alt=""/>
							</div>
							
						</div>
						<div className="workflow_item_content text-center">
							<div className="workflow_title">Choose a Location</div>
							<p className="workflow_text">Donec ullamcorper nulla non metus auctor fringi lla. Curabitur blandit tempus porttitor.</p>
						</div>
					</div>
				</div>

			
				<div className="col-lg-4 workflow_col">
					<div className="workflow_item">
						<div className="workflow_image_container d-flex flex-column align-items-center justify-content-center">
							<div className="workflow_image_background">
								<div className="workflow_circle_outer trans_200"></div>
								<div className="workflow_circle_inner trans_200"></div>
								<div className="workflow_num text-center trans_200"><span>02.</span></div>
							</div>
							<div className="workflow_image">
								<img src="images/workflow_2.png" alt=""/>
							</div>
							
						</div>
						<div className="workflow_item_content text-center">
							<div className="workflow_title">Find the Perfect Home</div>
							<p className="workflow_text">Donec ullamcorper nulla non metus auctor fringi lla. Curabitur blandit tempus porttitor.</p>
						</div>
					</div>
				</div>

			
				<div className="col-lg-4 workflow_col">
					<div className="workflow_item">
						<div className="workflow_image_container d-flex flex-column align-items-center justify-content-center">
							<div className="workflow_image_background">
								<div className="workflow_circle_outer trans_200"></div>
								<div className="workflow_circle_inner trans_200"></div>
								<div className="workflow_num text-center trans_200"><span>03.</span></div>
							</div>
							<div className="workflow_image">
								<img src="images/workflow_3.png" alt=""/>
							</div>
							
						</div>
						<div className="workflow_item_content text-center">
							<div className="workflow_title">Move in your new life</div>
							<p className="workflow_text">Donec ullamcorper nulla non metus auctor fringi lla. Curabitur blandit tempus porttitor.</p>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

    <div className="cities">
		<div className="cities_background"></div>
		<div className="container">
			<div className="row">
				<div className="col">
					<div className="section_title text-center">
						<h3>cities clients prefer</h3>
						<span className="section_subtitle">What you need to do</span>
					</div>
				</div>
			</div>

			<div className="row">
				<div className="col">

					
		
					<div className="cities_slider_container">
						<div className="owl-carousel owl-theme cities_slider">
							
					
							<div className="owl-item city_item">
								<a href="#">
									<div className="city_image">
										<img src="images/city_1.jpg" alt=""/>
									</div>
									<div className="city_details"><img src="images/search.png" alt=""/></div>
									<div className="city_name"><span>miami</span></div>
								</a>
							</div>

					
							<div className="owl-item city_item">
								<a href="#">
									<div className="city_image">
										<img src="images/city_2.jpg" alt=""/>
									</div>
									<div className="city_details"><img src="images/search.png" alt=""/></div>
									<div className="city_name"><span>dublin</span></div>
								</a>
							</div>

						
							<div className="owl-item city_item">
								<a href="#">
									<div className="city_image">
										<img src="images/city_3.jpg" alt=""/>
									</div>
									<div className="city_details"><img src="images/search.png" alt=""/></div>
									<div className="city_name"><span>vienna</span></div>
								</a>
							</div>

					
							<div className="owl-item city_item">
								<a href="#">
									<div className="city_image">
										<img src="images/city_4.jpg" alt=""/>
									</div>
									<div className="city_details"><img src="images/search.png" alt=""/></div>
									<div className="city_name"><span>marbella</span></div>
								</a>
							</div>

					
							<div className="owl-item city_item">
								<a href="#">
									<div className="city_image">
										<img src="images/city_5.jpg" alt=""/>
									</div>
									<div className="city_details"><img src="images/search.png" alt=""/></div>
									<div className="city_name"><span>new york</span></div>
								</a>
							</div>

							
							<div className="owl-item city_item">
								<a href="#">
									<div className="city_image">
										<img src="images/city_6.jpg" alt=""/>
									</div>
									<div className="city_details"><img src="images/search.png" alt=""/></div>
									<div className="city_name"><span>geneva</span></div>
								</a>
							</div>
							
						</div>
						
						<div className="cities_prev cities_nav d-flex flex-row align-items-center justify-content-center trans_200">
							<img src="images/nav_left.png" alt=""/>
						</div>

						<div className="cities_next cities_nav d-flex flex-row align-items-center justify-content-center trans_200">
							<img src="images/nav_right.png" alt=""/>
						</div>

					</div>

				</div>
					
			</div>

		</div>
	</div>

    <div className="cta_1">
		<div className="cta_1_background" style={{backgroundImage:"url(images/cta_1.jpg)"}}></div>
		<div className="container">
			<div className="row">
				<div className="col">
					
					<div className="cta_1_content d-flex flex-lg-row flex-column align-items-center justify-content-start">
						<h3 className="cta_1_text text-lg-left text-center">Do you want to talk with one of our <span>real estate experts?</span></h3>
						<div className="cta_1_phone">Call now:   +0080 234 567 84441</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>

    <Newsletter />

	<Footer />
                </div>
                {/* THE END */}
            </div>
        )
    }
}

export default HomePage;