import React, { Component } from 'react'
import Footer from '../Layout/Footer'
import Newsletter from '../Layout/Newsletter';
import Navbar from '../Layout/Navbar';

 class Single extends Component {
    render() {
        return (
            <div>
                <div class="super_container">
                <div class="home1">
		
		<div class="home1_background" style={{backgroundImage:"url(images/listings_single.jpg)"}}></div>
		
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home1_content">
						<div class="home1_title">
							<h2>single listings</h2>
						</div>
						<div class="breadcrumbs">
							<span><a href="index.html">Home</a></span>
							<span><a href="#"> Search</a></span>
							<span><a href="listings.html"> Listings</a></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

    <Navbar />

	<div class="listing2">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					
					
					<div class="listing2_title_container">
						<div class="listing2_title">House in west California</div>
						<p class="listing2_text">Donec ullamcorper nulla non metus auctor fringi lla.Curabitur blandit tempus porttitor.</p>
						<div class="room2_tags">
							<span class="room2_tag"><a href="#">Hottub</a></span>
							<span class="room2_tag"><a href="#">Swimming Pool</a></span>
							<span class="room2_tag"><a href="#">Garden</a></span>
							<span class="room2_tag"><a href="#">Patio</a></span>
							<span class="room2_tag"><a href="#">Hard Wood Floor</a></span>
						</div>
					</div>
				</div>
				
				
				<div class="col-lg-4 listing2_price_col clearfix">
					<div class="featured2_card_box d-flex flex-row align-items-center trans_300 float-lg-right">
						<img src="images/tag.svg" alt="https://www.flaticon.com/authors/lucy-g"/>
						<div class="featured2_card_box_content">
							<div class="featured2_card_price_title trans_300">For Sale</div>
							<div class="featured2_card_price trans_300">$540,000</div>
						</div>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="col">
					
				

					<div class="listing_slider_container">
						<div class="owl-carousel owl-theme listing_slider">
							
							
							<div class="owl-item listing_slider_item">
								<img src="images/listing_slider_1.jpg" alt="https://unsplash.com/@astute"/>
							</div>

						
							<div class="owl-item listing_slider_item">
								<img src="images/listing_slider_1.jpg" alt="https://unsplash.com/@astute"/>
							</div>

							
							<div class="owl-item listing_slider_item">
								<img src="images/listing_slider_1.jpg" alt="https://unsplash.com/@astute"/>
							</div>

							
							<div class="owl-item listing_slider_item">
								<img src="images/listing_slider_1.jpg" alt="https://unsplash.com/@astute"/>
							</div>

							
							<div class="owl-item listing_slider_item">
								<img src="images/listing_slider_1.jpg" alt="https://unsplash.com/@astute"/>
							</div>
							
						</div>

						<div class="listing_slider_nav listing_slider_prev d-flex flex-row align-items-center justify-content-center trans_200">
							<img src="images/nav_left.png" alt=""/>
						</div>

						<div class="listing_slider_nav listing_slider_next d-flex flex-row align-items-center justify-content-center trans_200">
							<img src="images/nav_right.png" alt=""/>
						</div>

					</div>

				</div>
			</div>
					

			<div class="row listing2_content_row">
				
			

				<div class="col-lg-4 sidebar_col">
				

					<div class="search1_box">

						<div class="search1_box_content">

							
							<div class="search1_box_title text-center">
								<div class="search1_box_title_inner">
									<div class="search1_box_title_icon d-flex flex-column align-items-center justify-content-center"><img src="images/search.png" alt=""/></div>
									<span>search your home</span>
								</div>
							</div>

						
							<form class="search1_form" action="#">
								<div class="search1_box_container">
									<ul class="dropdown1_row clearfix">
										<li class="dropdown1_item">
											<div class="dropdown1_item_title">Keywords</div>
											<select name="keywords" id="keywords" class="dropdown1_item_select">
												<option>Any</option>
												<option>Keyword 1</option>
												<option>Keyword 2</option>
											</select>
										</li>
										<li class="dropdown1_item">
											<div class="dropdown1_item_title">Property ID</div>
											<select name="property_ID" id="property_ID" class="dropdown1_item_select">
												<option>Any</option>
												<option>ID 1</option>
												<option>ID 2</option>
											</select>
										</li>
										<li class="dropdown1_item">
											<div class="dropdown1_item_title">Property Status</div>
											<select name="property_status" id="property_status" class="dropdown1_item_select">
												<option>Any</option>
												<option>Status 1</option>
												<option>Status 2</option>
											</select>
										</li>
										<li class="dropdown1_item">
											<div class="dropdown1_item_title">Location</div>
											<select name="property_location" id="property_location" class="dropdown1_item_select">
												<option>Any</option>
												<option>Location 1</option>
												<option>Location 2</option>
											</select>
										</li>
										<li class="dropdown1_item">
											<div class="dropdown1_item_title">Property Type</div>
											<select name="property_type" id="property_type" class="dropdown1_item_select">
												<option>Any</option>
												<option>Type 1</option>
												<option>Type 2</option>
											</select>
										</li>
										<li class="dropdown1_item dropdown1_item_half">
											<div class="dropdown1_item_title">Bedrooms no</div>
											<select name="bedrooms_no" id="bedrooms_no" class="dropdown1_item_select">
												<option>Any</option>
												<option>1</option>
												<option>2</option>
											</select>
										</li>
										<li class="dropdown1_item dropdown1_item_half">
											<div class="dropdown1_item_title">Bathrooms no</div>
											<select name="bathrooms_no" id="bathrooms_no" class="dropdown1_item_select">
												<option>Any</option>
												<option>1</option>
												<option>2</option>
											</select>
										</li>
										<li class="dropdown1_item dropdown1_item_half">
											<div class="dropdown1_item_title">Min Price</div>
											<select name="min_price" id="min_price" class="dropdown1_item_select">
												<option>Any</option>
												<option>$10000</option>
												<option>$20000</option>
											</select>
										</li>
										<li class="dropdown1_item dropdown1_item_half">
											<div class="dropdown1_item_title">Max Price</div>
											<select name="max_price" id="max_price" class="dropdown1_item_select">
												<option>Any</option>
												<option>$1000000</option>
												<option>$2000000</option>
											</select>
										</li>
										<li class="dropdown1_item dropdown1_item_half">
											<div class="dropdown1_item_title">Min Sq Ft</div>
											<select name="min_sq_ft" id="min_sq_ft" class="dropdown1_item_select">
												<option>Any</option>
												<option>Any</option>
												<option>Any</option>
											</select>
										</li>
										<li class="dropdown1_item dropdown1_item_half">
											<div class="dropdown1_item_title">Max Sq Ft</div>
											<select name="max_sq_ft" id="max_sq_ft" class="dropdown1_item_select">
												<option>Any</option>
												<option>Any</option>
												<option>Any</option>
											</select>
										</li>
									</ul>
								</div>

								<div class="search1_features_container">
									<div class="search1_features_trigger">
										<a href="#">Specific features</a>
									</div>
									<ul class="search1_features clearfix">
										<li class="search1_features_item">
											<div>
												<input type="checkbox" id="search1_features_1" class="search1_features_cb"/>
												<label for="search1_features_1">Feature 1</label>
											</div>	
										</li>
										<li class="search1_features_item">
											<div>
												<input type="checkbox" id="search1_features_2" class="search1_features_cb"/>
												<label for="search1_features_2">Feature 2</label>
											</div>
										</li>
										<li class="search1_features_item">
											<div>
												<input type="checkbox" id="search1_features_3" class="search1_features_cb"/>
												<label for="search1_features_3">Feature 3</label>
											</div>
										</li>
										<li class="search1_features_item">
											<div>
												<input type="checkbox" id="search1_features_4" class="search1_features_cb"/>
												<label for="search1_features_4">Feature 4</label>
											</div>
										</li>
										<li class="search1_features_item">
											<div>
												<input type="checkbox" id="search1_features_5" class="search1_features_cb"/>
												<label for="search1_features_5">Feature 5</label>
											</div>
										</li>
										<li class="search1_features_item">
											<div>
												<input type="checkbox" id="search1_features_6" class="search1_features_cb"/>
												<label for="search1_features_6">Feature 6</label>
											</div>
										</li>
										<li class="search1_features_item">
											<div>
												<input type="checkbox" id="search1_features_7" class="search1_features_cb"/>
												<label for="search1_features_7">Feature 7</label>
											</div>
										</li>
										<li class="search1_features_item">
											<div>
												<input type="checkbox" id="search1_features_8" class="search1_features_cb"/>
												<label for="search1_features_8">Feature 8</label>
											</div>
										</li>
										<li class="search1_features_item">
											<div>
												<input type="checkbox" id="search1_features_9" class="search1_features_cb"/>
												<label for="search1_features_9">Feature 9</label>
											</div>
										</li>
										<li class="search1_features_item">
											<div>
												<input type="checkbox" id="search1_features_10" class="search1_features_cb"/>
												<label for="search1_features_10">Feature 10</label>
											</div>
										</li>
									</ul>

									<div class="search1_button">
										<input value="search" type="submit" class="search1_submit_button"/>
									</div>
								</div>
							</form>
						</div>	
					</div>

					<div class="hello">
						<div class="footer_col_title">say hello</div>
						<div class="footer_contact_form_container">
							<form id="hello_contact_form" class="footer_contact_form" action="post">
								<input id="hello_contact_form_name" class="input_field contact_form_name" type="text" placeholder="Name" required="required" data-error="Name is required."/>
								<input id="hello_contact_form_email" class="input_field contact_form_email" type="email" placeholder="E-mail" required="required" data-error="Valid email is required."/>
								<textarea id="hello_contact_form_message" class="text_field contact_form_message" name="message" placeholder="Message" required="required" data-error="Please, write us a message."></textarea>
								<button id="hello_contact_send_btn" type="submit" class="contact_send_btn trans_200" value="Submit">send</button>
							</form>
						</div>
					</div>
				</div>

				

				<div class="col-lg-8 listing2_col">

					<div class="listing2_details">
						<div class="listing2_subtitle">Extra Facilities</div>
						<p class="listing2_details_text">Donec ullamcorper nulla non metus auctor fringi lla. Curabitur blandit tempus porttitor.</p>
						<div class="rooms2">

							<div class="room2">
								<span class="room2_title">Bedrooms</span>
								<div class="room2_content">
									<div class="room2_image"><img src="images/bedroom.png" alt=""/></div>
									<span class="room2_number">4</span>
								</div>
							</div>

							<div class="room2">
								<span class="room2_title">Bathrooms</span>
								<div class="room2_content">
									<div class="room2_image"><img src="images/shower.png" alt=""/></div>
									<span class="room2_number">3</span>
								</div>
							</div>

							<div class="room2">
								<span class="room2_title">Area</span>
								<div class="room2_content">
									<div class="room2_image"><img src="images/area.png" alt=""/></div>
									<span class="room2_number">7100 Sq Ft</span>
								</div>
							</div>

							<div class="room2">
								<span class="room2_title">Patio</span>
								<div class="room2_content">
									<div class="room2_image"><img src="images/patio.png" alt=""/></div>
									<span class="room2_number">1</span>
								</div>
							</div>

							<div class="room2">
								<span class="room_title">Garage</span>
								<div class="room2_content">
									<div class="room2_image"><img src="images/garage.png" alt=""/></div>
									<span class="room2_number">2</span>
								</div>
							</div>

						</div>
						
					</div>
					
					
					<div class="listing2_description">
						<div class="listing2_subtitle">Description</div>
						<p class="listing2_description_text">Donec ullamcorper nulla non metus auctor fringi lla. Curabitur blandit tempus porttitor.Sed lectus urna, ultricies sit amet risus eget, euismod imperdiet augue. Duis imperdiet, purus a pellentesque sodales, sapien mauris rhoncus eros, ac blandit elit leo ac diam. Sed lectus urna, ultricies sit amet risus eget, euismod imperdiet augue. Duis imperdiet, purus a pellentesque sodales, sapien mauris rhoncus eros, ac blandit elit leo ac diam</p>
					</div>
					
					
					<div class="listing2_additional_details">
						<div class="listing2_subtitle">Additional Details</div>
						<ul class="additional_details_list">
							<li class="additional_detail"><span>bedroom features:</span> Main Floor Master Bedroom, Walk-In Closet</li>
							<li class="additional_detail"><span>dining area:</span> Breakfast Counter/Bar, Living/Dining Combo</li>
							<li class="additional_detail"><span>doors & windows:</span> Bay Window</li>
							<li class="additional_detail"><span>entry location:</span> Mid Level</li>
							<li class="additional_detail"><span>floors:</span> Raised Foundation, Vinyl Tile, Wall-to-Wall Carpet, Wood</li>
						</ul>
					</div>
					
					
					<div class="listing2_video">
						<div class="listing2_subtitle">Property Video</div>
						<div class="listing2_video_link">
							<a class="video" href="https://vimeo.com/99340873" title=""><img src="images/listing_video.jpg" alt="https://www.pexels.com/u/binyaminmellish/"/></a>
							<div class="video_play"><img src="images/play.svg" alt=""/></div>
						</div>
					</div>

					
					<div class="listing2_map">
						<div class="listing2_subtitle">Property on map</div>
						<div id="google_map">
							<div class="map_container">
								<div id="map"></div>
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>
	</div>
    
	<Newsletter />

	<Footer />
                </div>
                  {/* THE END */}
            </div>
        )
    }
}
export default Single;