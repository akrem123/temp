import React, { Component } from 'react'
import Footer from '../Layout/Footer'
import Newsletter from '../Layout/Newsletter';
import Navbar from '../Layout/Navbar';

 class Listings extends Component {
    render() {
        return (
            <div>
                <div className="super_container">
                <div className="home1">
		
		<div className="home1_background" style={{backgroundImage:"url(images/listings.jpg)"}}></div>
		
		<div className="container">
			<div className="row">
				<div className="col">
					<div className="home1_content">
						<div className="home1_title">
							<h2>listings</h2>
						</div>
						<div className="breadcrumbs">
							<span><a href="/">Home</a></span>
							<span><a href="#"> Search</a></span>
							<span><a href="#"> Listings</a></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    
	<Navbar />

    <div className="listings1">
		<div className="container">
			<div className="row">
				
				

				<div className="col-lg-4 sidebar_col">
					
					<div className="search1_box">

						<div className="search1_box_content">

							
							<div className="search1_box_title text-center">
								<div className="search1_box_title_inner">
									<div className="search1_box_title_icon d-flex flex-column align-items-center justify-content-center"><img src="images/search.png" alt=""/></div>
									<span>search your home</span>
								</div>
							</div>

						
							<form className="search1_form" action="#">
								<div className="search1_box_container">
									<ul className="dropdown1_row clearfix">
										<li className="dropdown1_item">
											<div className="dropdown1_item_title">Keywords</div>
											<select name="keywords" id="keywords" className="dropdown1_item_select">
												<option>Any</option>
												<option>Keyword 1</option>
												<option>Keyword 2</option>
											</select>
										</li>
										<li className="dropdown1_item">
											<div className="dropdown1_item_title">Property ID</div>
											<select name="property_ID" id="property_ID" className="dropdown1_item_select">
												<option>Any</option>
												<option>ID 1</option>
												<option>ID 2</option>
											</select>
										</li>
										<li className="dropdown1_item">
											<div className="dropdown1_item_title">Property Status</div>
											<select name="property_status" id="property_status" className="dropdown1_item_select">
												<option>Any</option>
												<option>Status 1</option>
												<option>Status 2</option>
											</select>
										</li>
										<li className="dropdown1_item">
											<div className="dropdown1_item_title">Location</div>
											<select name="property_location" id="property_location" className="dropdown1_item_select">
												<option>Any</option>
												<option>Location 1</option>
												<option>Location 2</option>
											</select>
										</li>
										<li className="dropdown1_item">
											<div className="dropdown1_item_title">Property Type</div>
											<select name="property_type" id="property_type" className="dropdown1_item_select">
												<option>Any</option>
												<option>Type 1</option>
												<option>Type 2</option>
											</select>
										</li>
										<li className="dropdown1_item dropdown1_item_half">
											<div className="dropdown1_item_title">Bedrooms no</div>
											<select name="bedrooms_no" id="bedrooms_no" className="dropdown1_item_select">
												<option>Any</option>
												<option>1</option>
												<option>2</option>
											</select>
										</li>
										<li className="dropdown1_item dropdown1_item_half">
											<div className="dropdown1_item_title">Bathrooms no</div>
											<select name="bathrooms_no" id="bathrooms_no" className="dropdown1_item_select">
												<option>Any</option>
												<option>1</option>
												<option>2</option>
											</select>
										</li>
										<li className="dropdown1_item dropdown1_item_half">
											<div className="dropdown1_item_title">Min Price</div>
											<select name="min_price" id="min_price" className="dropdown1_item_select">
												<option>Any</option>
												<option>$10000</option>
												<option>$20000</option>
											</select>
										</li>
										<li className="dropdown1_item dropdown1_item_half">
											<div className="dropdown1_item_title">Max Price</div>
											<select name="max_price" id="max_price" className="dropdown1_item_select">
												<option>Any</option>
												<option>$1000000</option>
												<option>$2000000</option>
											</select>
										</li>
										<li className="dropdown1_item dropdown1_item_half">
											<div className="dropdown1_item_title">Min Sq Ft</div>
											<select name="min_sq_ft" id="min_sq_ft" className="dropdown1_item_select">
												<option>Any</option>
												<option>Any</option>
												<option>Any</option>
											</select>
										</li>
										<li className="dropdown1_item dropdown1_item_half">
											<div className="dropdown1_item_title">Max Sq Ft</div>
											<select name="max_sq_ft" id="max_sq_ft" className="dropdown1_item_select">
												<option>Any</option>
												<option>Any</option>
												<option>Any</option>
											</select>
										</li>
									</ul>
								</div>

								<div className="search1_features_container">
									<div className="search1_features_trigger">
										<a href="#">Specific features</a>
									</div>
									<ul className="search1_features clearfix">
										<li className="search1_features_item">
											<div>
												<input type="checkbox" id="search1_features_1" className="search1_features_cb"/>
												<label for="search1_features_1">Feature 1</label>
											</div>	
										</li>
										<li className="search1_features_item">
											<div>
												<input type="checkbox" id="search1_features_2" className="search1_features_cb"/>
												<label for="search1_features_2">Feature 2</label>
											</div>
										</li>
										<li className="search_features_item">
											<div>
												<input type="checkbox" id="search1_features_3" className="search1_features_cb"/>
												<label for="search1_features_3">Feature 3</label>
											</div>
										</li>
										<li className="search1_features_item">
											<div>
												<input type="checkbox" id="search1_features_4" className="search1_features_cb"/>
												<label for="search1_features_4">Feature 4</label>
											</div>
										</li>
										<li className="search1_features_item">
											<div>
												<input type="checkbox" id="search1_features_5" className="search1_features_cb"/>
												<label for="search1_features_5">Feature 5</label>
											</div>
										</li>
										<li className="search1_features_item">
											<div>
												<input type="checkbox" id="search1_features_6" className="search1_features_cb"/>
												<label for="search1_features_6">Feature 6</label>
											</div>
										</li>
										<li className="search_features_item">
											<div>
												<input type="checkbox" id="search1_features_7" className="search1_features_cb"/>
												<label for="search1_features_7">Feature 7</label>
											</div>
										</li>
										<li className="search1_features_item">
											<div>
												<input type="checkbox" id="search1_features_8" className="search1_features_cb"/>
												<label for="search1_features_8">Feature 8</label>
											</div>
										</li>
										<li className="search1_features_item">
											<div>
												<input type="checkbox" id="search1_features_9" className="search1_features_cb"/>
												<label for="search1_features_9">Feature 9</label>
											</div>
										</li>
										<li className="search1_features_item">
											<div>
												<input type="checkbox" id="search1_features_10" className="search1_features_cb"/>
												<label for="search1_features_10">Feature 10</label>
											</div>
										</li>
									</ul>

									<div className="search1_button">
										<input value="search" type="submit" className="search1_submit_button"/>
									</div>
								</div>
							</form>
						</div>	
					</div>
				</div>

				

				<div className="col-lg-8 listings1_col">

					

					<div className="listing1_item">
						<div className="listing1_item_inner d-flex flex-md-row flex-column trans_300">
							<div className="listing1_image_container">
								<div className="listing1_image">
									
									<div className="listing1_background" style={{backgroundImage:"url(images/listing_1.jpg)"}}></div>
								</div>
								<div className="featured1_card_box d-flex flex-row align-items-center trans_300">
									<img src="images/tag.svg" alt="https://www.flaticon.com/authors/lucy-g"/>
									<div className="featured1_card_box_content">
										<div className="featured1_card_price_title trans_300">For Sale</div>
										<div className="featured1_card_price trans_300">$540,000</div>
									</div>
								</div>
							</div>
							<div className="listing1_content">
								<div className="listing1_title"><a href="listings1_single.html">House in West California</a></div>
								<div className="listing1_text">Donec ullamcorper nulla non metus auctor fringi lla. Curabitur blandit tempus porttitor.</div>
								<div className="rooms1">

									<div className="room1">
										<span className="room1_title">Bedrooms</span>
										<div className="room1_content">
											<div className="room1_image"><img src="images/bedroom.png" alt=""/></div>
											<span className="room1_number">4</span>
										</div>
									</div>

									<div className="room1">
										<span className="room1_title">Bathrooms</span>
										<div className="room1_content">
											<div className="room1_image"><img src="images/shower.png" alt=""/></div>
											<span className="room1_number">3</span>
										</div>
									</div>

									<div className="room1">
										<span className="room1_title">Area</span>
										<div className="room1_content">
											<div className="room1_image"><img src="images/area.png" alt=""/></div>
											<span className="room1_number">7100 Sq Ft</span>
										</div>
									</div>

								</div>

								<div className="room1_tags">
									<span className="room1_tag"><a href="#">Hottub</a></span>
									<span className="room1_tag"><a href="#">Swimming Pool</a></span>
									<span className="room1_tag"><a href="#">Garden</a></span>
									<span className="room1_tag"><a href="#">Patio</a></span>
									<span className="room1_tag"><a href="#">Hard Wood Floor</a></span>
								</div>
							</div>
						</div>
					</div>

				

					<div className="listing1_item">
						<div className="listing1_item_inner d-flex flex-md-row flex-column trans_300">
							<div className="listing1_image_container">
								<div className="listing1_image">
								
									<div className="listing1_background" style={{backgroundImage:"url(images/listing_2.jpg)"}}></div>
								</div>
								<div className="featured1_card_box d-flex flex-row align-items-center trans_300">
									<img src="images/tag.svg" alt="https://www.flaticon.com/authors/lucy-g"/>
									<div className="featured1_card_box_content">
										<div className="featured1_card_price_title trans_300">For Sale</div>
										<div className="featured1_card_price trans_300">$540,000</div>
									</div>
								</div>
							</div>
							<div className="listing1_content">
								<div className="listing1_title"><a href="listings1_single.html">Villa in Milano</a></div>
								<div className="listing1_text">Donec ullamcorper nulla non metus auctor fringi lla. Curabitur blandit tempus porttitor.</div>
								<div className="rooms1">

									<div className="room1">
										<span className="room1_title">Bedrooms</span>
										<div className="room1_content">
											<div className="room1_image"><img src="images/bedroom.png" alt=""/></div>
											<span className="room1_number">4</span>
										</div>
									</div>

									<div className="room1">
										<span className="room1_title">Bathrooms</span>
										<div className="room1_content">
											<div className="room1_image"><img src="images/shower.png" alt=""/></div>
											<span className="room1_number">3</span>
										</div>
									</div>

									<div className="room1">
										<span className="room1_title">Area</span>
										<div className="room1_content">
											<div className="room1_image"><img src="images/area.png" alt=""/></div>
											<span className="room1_number">7100 Sq Ft</span>
										</div>
									</div>

								</div>

								<div className="room1_tags">
									<span className="room1_tag"><a href="#">Hottub</a></span>
									<span className="room1_tag"><a href="#">Swimming Pool</a></span>
									<span className="room1_tag"><a href="#">Garden</a></span>
									<span className="room1_tag"><a href="#">Patio</a></span>
									<span className="room1_tag"><a href="#">Hard Wood Floor</a></span>
								</div>
							</div>
						</div>
							
					</div>

			

					<div className="listing1_item">
						<div className="listing1_item_inner d-flex flex-md-row flex-column trans_300">
							<div className="listing1_image_container">
								<div className="listing1_image">
									
									<div className="listing1_background" style={{backgroundImage:"url(images/listing_3.jpg)"}}></div>
								</div>
								<div className="featured1_card_box d-flex flex-row align-items-center trans_300">
									<img src="images/tag.svg" alt="https://www.flaticon.com/authors/lucy-g"/>
									<div className="featured1_card_box_content">
										<div className="featured1_card_price_title trans_300">For Sale</div>
										<div className="featured1_card_price trans_300">$540,000</div>
									</div>
								</div>
							</div>
							<div className="listing1_content">
								<div className="listing1_title"><a href="listings_single.html">Villa in West Barcelona</a></div>
								<div className="listing1_text">Donec ullamcorper nulla non metus auctor fringi lla. Curabitur blandit tempus porttitor.</div>
								<div className="rooms1">

									<div className="room1">
										<span className="room1_title">Bedrooms</span>
										<div className="room1_content">
											<div className="room1_image"><img src="images/bedroom.png" alt=""/></div>
											<span className="room1_number">4</span>
										</div>
									</div>

									<div className="room1">
										<span className="room1_title">Bathrooms</span>
										<div className="room11_content">
											<div className="room1_image"><img src="images/shower.png" alt=""/></div>
											<span className="room1_number">3</span>
										</div>
									</div>

									<div className="room1">
										<span className="room1_title">Area</span>
										<div className="room1_content">
											<div className="room1_image"><img src="images/area.png" alt=""/></div>
											<span className="room1_number">7100 Sq Ft</span>
										</div>
									</div>

								</div>

								<div className="room1_tags">
									<span className="room1_tag"><a href="#">Hottub</a></span>
									<span className="room1_tag"><a href="#">Swimming Pool</a></span>
									<span className="room1_tag"><a href="#">Garden</a></span>
									<span className="room1_tag"><a href="#">Patio</a></span>
									<span className="room1_tag"><a href="#">Hard Wood Floor</a></span>
								</div>
							</div>
						</div>
					</div>

			

					<div className="listing1_item">
						<div className="listing1_item_inner d-flex flex-md-row flex-column trans_300">
							<div className="listing1_image_container">
								<div className="listing1_image">
									
									<div className="listing1_background" style={{backgroundImage:"url(images/listing_4.jpg)"}}></div>
								</div>
								<div className="featured1_card_box d-flex flex-row align-items-center trans_300">
									<img src="images/tag.svg" alt="https://www.flaticon.com/authors/lucy-g"/>
									<div className="featured1_card_box_content">
										<div className="featured1_card_price_title trans_300">For Sale</div>
										<div className="featured1_card_price trans_300">$540,000</div>
									</div>
								</div>
							</div>
							<div className="listing1_content">
								<div className="listing1_title"><a href="listings_single.html">Villa in Milano</a></div>
								<div className="listing1_text">Donec ullamcorper nulla non metus auctor fringi lla. Curabitur blandit tempus porttitor.</div>
								<div className="rooms1">

									<div className="room1">
										<span className="room1_title">Bedrooms</span>
										<div className="room1_content">
											<div className="room1_image"><img src="images/bedroom.png" alt=""/></div>
											<span className="room1_number">4</span>
										</div>
									</div>

									<div className="room1">
										<span className="room1_title">Bathrooms</span>
										<div className="room1_content">
											<div className="room1_image"><img src="images/shower.png" alt=""/></div>
											<span className="room1_number">3</span>
										</div>
									</div>

									<div className="room1">
										<span className="room1_title">Area</span>
										<div className="room1_content">
											<div className="room1_image"><img src="images/area.png" alt=""/></div>
											<span className="room1_number">7100 Sq Ft</span>
										</div>
									</div>

								</div>

								<div className="room1_tags">
									<span className="room1_tag"><a href="#">Hottub</a></span>
									<span className="room1_tag"><a href="#">Swimming Pool</a></span>
									<span className="room1_tag"><a href="#">Garden</a></span>
									<span className="room1_tag"><a href="#">Patio</a></span>
									<span className="room1_tag"><a href="#">Hard Wood Floor</a></span>
								</div>
							</div>
						</div>
					</div>

		

					<div className="listing1_item">
						<div className="listing1_item_inner d-flex flex-md-row flex-column trans_300">
							<div className="listing1_image_container">
								<div className="listing1_image">
									
									<div className="listing1_background" style={{backgroundImage:"url(images/listing_5.jpg)"}}></div>
								</div>
								<div className="featured1_card_box d-flex flex-row align-items-center trans_300">
									<img src="images/tag.svg" alt="https://www.flaticon.com/authors/lucy-g"/>
									<div className="featured1_card_box_content">
										<div className="featured1_card_price_title trans_300">For Sale</div>
										<div className="featured1_card_price trans_300">$540,000</div>
									</div>
								</div>
							</div>
							<div className="listing1_content">
								<div className="listing1_title"><a href="listings1_single.html">Villa in West Barcelona</a></div>
								<div className="listing1_text">Donec ullamcorper nulla non metus auctor fringi lla. Curabitur blandit tempus porttitor.</div>
								<div className="rooms1">

									<div className="room1">
										<span className="room1_title">Bedrooms</span>
										<div className="room1_content">
											<div className="room1_image"><img src="images/bedroom.png" alt=""/></div>
											<span className="room1_number">4</span>
										</div>
									</div>

									<div className="room1">
										<span className="room1_title">Bathrooms</span>
										<div className="room1_content">
											<div className="room1_image"><img src="images/shower.png" alt=""/></div>
											<span className="room1_number">3</span>
										</div>
									</div>

									<div className="room1">
										<span className="room1_title">Area</span>
										<div className="room1_content">
											<div className="room1_image"><img src="images/area.png" alt=""/></div>
											<span className="room1_number">7100 Sq Ft</span>
										</div>
									</div>

								</div>

								<div className="room1_tags">
									<span className="room1_tag"><a href="#">Hottub</a></span>
									<span className="room1_tag"><a href="#">Swimming Pool</a></span>
									<span className="room1_tag"><a href="#">Garden</a></span>
									<span className="room1_tag"><a href="#">Patio</a></span>
									<span className="room1_tag"><a href="#">Hard Wood Floor</a></span>
								</div>
							</div>
						</div>
					</div>

				</div>

			</div>

			<div className="row">
				<div className="col clearfix">
					<div className="listings1_nav">
						<ul>
							<li className="listings1_nav_item active"><a href="#">01.</a></li>
							<li className="listings1_nav_item"><a href="#">02.</a></li>
							<li className="listings1_nav_item"><a href="#">03.</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

    <Newsletter />

	<Footer />
                </div>
                 {/* THE END */}
            </div>
        )
    }
}
export default Listings;