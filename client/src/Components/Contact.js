import React, { Component } from 'react'
import Footer from '../Layout/Footer'
import Newsletter from '../Layout/Newsletter'
import Navbar from '../Layout/Navbar'

class Contact extends Component {
    render() {
        return (
            <div>
                <div className="super_container">
                <div className="home1">
		
		<div className="home1_background" style={{backgroundImage:"url(images/contact.jpg)"}}></div>
		
		<div className="container">
			<div className="row">
				<div className="col">
					<div className="home1_content">
						<div className="home1_title">
							<h2>Contact</h2>
						</div>
						<div className="breadcrumbs">
							<span><a href="index.html">Home</a></span>
							<span><a href="#"> Contact</a></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

   <Navbar />

    <div className="contact">
		<div className="container">
			<div className="row">
				
				<div className="col-lg-6 contact_col">
					<div className="estate_contact_form">
						<div className="contact_title">say hello</div>
						<div className="estate_contact_form_container">
							<form id="estate_contact_form" className="estate_contact_form" action="post">
								<input id="estate_contact_form_name" className="estate_input_field estate_contact_form_name" type="text" placeholder="Name" required="required" data-error="Name is required."/>
								<input id="estate_contact_form_email" className="estate_input_field estate_contact_form_email" type="email" placeholder="E-mail" required="required" data-error="Valid email is required."/>
								<input id="estate_contact_form_subject" className="estate_input_field estate_contact_form_subject" type="email" placeholder="Subject" required="required" data-error="Subject is required."/>
								<textarea id="estate_contact_form_message" className="estate_text_field estate_contact_form_message" name="message" placeholder="Message" required="required" data-error="Please, write us a message."></textarea>
								<button id="estate_contact_send_btn" type="submit" className="estate_contact_send_btn trans_200" value="Submit">send</button>
							</form>
						</div>
					</div>
				</div>

				<div className="col-lg-3 contact_col">
					<div className="contact_title">contact info</div>
					<ul className="contact_info_list estate_contact">
						<li className="contact_info_item d-flex flex-row">
							<div><div className="contact_info_icon"><img src="images/placeholder.svg" alt=""/></div></div>
							<div className="contact_info_text">4127 Raoul Wallenber 45b-c Gibraltar</div>
						</li>
						<li className="contact_info_item d-flex flex-row">
							<div><div className="contact_info_icon"><img src="images/phone-call.svg" alt=""/></div></div>
							<div className="contact_info_text">2556-808-8613</div>
						</li>
						<li className="contact_info_item d-flex flex-row">
							<div><div className="contact_info_icon"><img src="images/message.svg" alt=""/></div></div>
							<div className="contact_info_text"><a href="mailto:contactme@gmail.com?Subject=Hello" target="_top">contactme@gmail.com</a></div>
						</li>
						<li className="contact_info_item d-flex flex-row">
							<div><div className="contact_info_icon"><img src="images/planet-earth.svg" alt=""/></div></div>
							<div className="contact_info_text"><a href="https://colorlib.com">www.colorlib.com</a></div>
						</li>
					</ul>
					<div className="estate_social">
						<ul className="estate_social_list">
							<li className="estate_social_item"><a href="#"><i className="fab fa-pinterest"></i></a></li>
							<li className="estate_social_item"><a href="#"><i className="fab fa-facebook-f"></i></a></li>
							<li className="estate_social_item"><a href="#"><i className="fab fa-twitter"></i></a></li>
							<li className="estate_social_item"><a href="#"><i className="fab fa-dribbble"></i></a></li>
							<li className="estate_social_item"><a href="#"><i className="fab fa-behance"></i></a></li>
						</ul>
					</div>
				</div>

				<div className="col-lg-3 contact_col">
					<div className="contact_title">about</div>
					<div className="estate_about_text">
						<p>Lorem ipsum dolor sit amet, cons ectetur  quis ferme adipiscing elit. Suspen dis se tellus eros, placerat quis ferme ntum et, adipiscingvive rra sit ipsum amet lacus. </p>
						<p>Nam gravida quis placerat quis fe rme ntum et ferme sadipiscinge te llus semper augue.</p>
					</div>
				</div>

			</div>

		</div>

		
		
		<div className="estate_map">
			<div id="google_map" className="google_map">
				<div className="map_container">
					<div id="map"></div>
				</div>
			</div>
		</div>

	</div>

    <Newsletter />

	<Footer />
                </div>
                 {/* THE END */}
            </div>
        )
    }
}
export default Contact